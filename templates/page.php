<?php if(isset($form_obj->severity)): ?>
<div class="alert alert-<?php echo $form_obj->severity; ?>">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<?php echo $form_obj->message; ?>
</div>
<?php endif;?>
<?php if(isset($form_obj->form_groups) && is_array($form_obj->form_groups)): ?>
<form method="post" class="form-horizontal" action="page.php" id="page_form">
<?php foreach($form_obj->hidden_elements as $hidden_form): ?>
	<input type="hidden" name="<?php echo $hidden_form->name; ?>" value="<?php echo $hidden_form->value; ?>" />
<?php endforeach; ?>
<?php foreach($form_obj->form_groups as $form_obj_group): ?>
	<fieldset>
		<legend><?php echo $form_obj_group->name; ?></legend>
<?php foreach($form_obj_group->elements as $form): ?>
	<div class="control-group">
		<?php if($form->type != 'submit' && isset($form->label) && !empty($form->label)): ?>
		<label class="control-label" for="<?php echo $form->name; ?>"><?php echo $form->label; ?></label>
		<?php endif; ?>
		<div class="controls">
		<?php if($form->type=='content'):?>
		<?php echo $form->value; ?>
		<?php endif; ?>
		<?php if(in_array($form->type, array('text', 'password', 'submit', 'email','tel','number'))): ?>
			<input type="<?php echo $form->type; ?>" name="<?php echo $form->name; ?>" id="<?php echo $form->name; ?>" value="<?php echo htmlentities($form->value); ?>" placeholder="<?php echo htmlentities($form->placeholder); ?>"  <?php if(is_array($form->attr) && count($form->attr)>0){echo implode(' ', $form->attr); } ?>/>
		<?php elseif ($form->type == 'textarea'): ?>
			<textarea name="<?php echo $form->name; ?>" id="<?php echo $form->name; ?>" placeholder="<?php echo htmlentities($form->placeholder); ?>" <?php if(is_array($form->attr) && count($form->attr)>0){echo implode(' ', $form->attr); } ?>><?php echo htmlentities($form->value); ?></textarea>
		<?php elseif ($form->type == 'select'): ?>
			<select name="<?php echo $form->name; ?>" id="<?php echo $form->name; ?>" <?php if(is_array($form->attr) && count($form->attr)>0){echo implode(' ', $form->attr); } ?>>
				<?php foreach($form->options as $key=>$option): ?>
				<option value="<?php echo ($key=="_empty_" ? "" : htmlentities($key)); ?>" <?php if($form->value==$key){ echo 'selected="selected"';}?>><?php echo $option; ?></option>
				<?php endforeach; ?>
			</select>
		<?php elseif ($form->type == 'checkbox'): ?>
			<?php foreach ($form->options as $key=>$option): ?>
			<label class="checkbox">
				<?php echo $option; ?>
				<input type="checkbox" name="<?php echo htmlentities($form->name); ?>" value="<?php echo htmlentities($key); ?>" <?php if(is_array($form->attr) && count($form->attr)>0){echo implode(' ', $form->attr); } ?> />
			</label>	
			<?php endforeach; ?>
		<?php elseif($form->type=='compound'): ?>
			<?php foreach($form->elements as $form_child): ?>
			<?php if ($form_child->type == 'select'): ?>
			<select name="<?php echo $form_child->name; ?>" id="<?php echo $form_child->name; ?>" <?php if(is_array($form_child->attr) && count($form_child->attr)>0){echo implode(' ', $form_child->attr); } ?>>
				<?php foreach($form_child->options as $key=>$option): ?>
				<option value="<?php echo ($key=="_empty_" ? "" : htmlentities($key)); ?>" <?php if($form_child->value==$key){ echo 'selected="selected"';}?>><?php echo $option; ?></option>
				<?php endforeach; ?>
			</select>
			<?php elseif(in_array($form_child->type, array('text', 'password', 'submit', 'email','tel','number'))): ?>
			<input type="<?php echo $form_child->type; ?>" name="<?php echo $form_child->name; ?>" id="<?php echo $form_child->name; ?>" value="<?php echo htmlentities($form_child->value); ?>" placeholder="<?php echo htmlentities($form_child->placeholder); ?>"  <?php if(is_array($form_child->attr) && count($form_child->attr)>0){echo implode(' ', $form_child->attr); } ?>/>
			<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>		
		</div>	
	</div>
<?php endforeach; ?>
	</fieldset>
<?php endforeach; ?>
</form>
<?php endif; ?>
<script>
if (top.location != location){
	top.location.href = document.location.href;
}
$(document).ready(function(){
	$(".phone, .numeric_only").keydown(function(event) {
		// Allow: backspace, delete, tab, escape, and enter
	    if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
	       // Allow: Ctrl+A
	       (event.keyCode == 65 && event.ctrlKey === true) ||
		   // Allow: home, end, left, right
	       (event.keyCode >= 35 && event.keyCode <= 39)) {
	                // let it happen, don't do anything
	                 return;
	        } else {
	            // Ensure that it is a number and stop the keypress
	            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	                event.preventDefault(); 
	            }   
	        }
	 });
	// $('#date_of_birth').datepicker();
});
</script>