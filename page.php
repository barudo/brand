<?php 
include_once('brand.php');
$brand = new Brand();

header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
header('Pragma: no-cache'); // HTTP 1.0.
header('Expires: 0'); // Proxies.
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Brands</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen" />
	    <link href="css/datepicker.css" rel="stylesheet" media="screen" />
	    <script src="js/jquery.min.js"></script>
	    <meta http-equiv='cache-control' content='no-cache'>
		<meta http-equiv='expires' content='0'>
		<meta http-equiv='pragma' content='0'>
	</head>
	<body>
		<div class="container">
			<h1 class="page-header">Sample Other Pages</h1>
			<?php 
				$brand->render();
			?>
		</div>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/bootstrap-datepicker.js"></script>
	</body>
</html>