<?php
require_once(dirname(__FILE__) . '/config.php');

class Brand {
	public $curl_connector;
	public $page_template;
	private $content;
	
	public function __construct(){
		$this->curl_connector = new curl_connector(BRAND_SLUG, BRAND_API_URL);
		$this->page_template = TEMPLATES_FOLDER . "/page.php";
		if(is_file(TEMPLATES_FOLDER . "/first_page.php"))
			$this->first_page_template = TEMPLATES_FOLDER . "/first_page.php"; else
			$this->first_page_template = TEMPLATES_FOLDER . "/page.php";
		$this->process();		
	}
	public function set_page_template($page_template){
		$this->page_template = $page_template;	
	}
	public function process(){
		if(isset($_POST['session_var'])){
			//A posting from client was done...
			$this->curl_connector->session_var = $_POST['session_var'];
			$this->process_postings($_POST);
		} else {
			$this->curl_connector->setup_session();
			$this->fetch_first_form();
		}
	}
	public function render($return=FALSE){
		if($return)
			return $this->content; else
			echo $this->content;
	}
	private function fetch_first_form(){
		$param = array(
				'session_var'=>$this->curl_connector->session_var,
				'slug'=>$this->curl_connector->brand_slug);
		$form_obj = json_decode($this->curl_connector->fetch('first_page', $param));
		if(is_object($form_obj) && $form_obj->success===TRUE){
			$form_obj->hidden_elements[] = (object)array(
					'name'=>'session_var',
					'value'=>$this->curl_connector->session_var); 
			ob_start();
			include_once($this->first_page_template);
			$this->content = ob_get_contents();
			ob_end_clean();
		} else {
			$this->content = "Error found while fetching form.";
		}
	}
	private function show_form($form_obj, $page=''){
		$form_obj->hidden_elements[] = (object)array(
					'name'=>'session_var',
					'value'=>$this->curl_connector->session_var); 
		ob_start();
		if(!empty($page) && is_file(TEMPLATES_FOLDER. "/page{$page}.php"))
			include_once(TEMPLATES_FOLDER. "/page{$page}.php"); else {
			if (isset($form_obj->template)){
				if(is_file(TEMPLATES_FOLDER . "/{$form_obj->template}.php"))
					include_once(TEMPLATES_FOLDER . "/{$form_obj->template}.php"); else
					include_once($this->page_template);
			} else {
				include_once($this->page_template);
			}
		}
		$this->content = ob_get_contents();	
		ob_end_clean();
	}
	private function process_postings($post){
		//submit form to API...
		$param = array(
				'session_var'=>$this->curl_connector->session_var,
				'slug'=>$this->curl_connector->brand_slug,
				);
		$param = array_merge($param, $post);
		 
		$form_obj = json_decode(utf8_encode($this->curl_connector->fetch('succeeding', $param)));
		$this->show_form($form_obj); 
	} 
}

class curl_connector {
	public $session_var;
	public $brand_slug;
	public $brand_api_url;
	
	public function __construct($brand_slug, $brand_api_url){
		$this->brand_slug = $brand_slug;
		$this->brand_api_url = $brand_api_url;
	}
	public function setup_session(){
		$return = json_decode($this->fetch('new_session', array('slug'=>$this->brand_slug)));
		if(is_object($return) && $return->success){
			$this->session_var = $return->session_variable;
		} else {
			$this->session_var = '';
		}
	}
	public function fetch($action, $params){
		$params['action'] = $action;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->brand_api_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
		if(preg_match('!https://!', $this->brand_api_url)){
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		}
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
		curl_setopt($ch, CURLOPT_POST, 1);
		$result = curl_exec($ch);
		//echo $result;
		return $result;
	}
}